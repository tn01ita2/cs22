#define FUSE_USE_VERSION 30

#include <fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <errno.h>
#include <stddef.h>
#include <assert.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <arpa/inet.h>

#define PORT 4444

int clientSocket, ret=-1, num_of_files= 0;
struct sockaddr_in serverAddr;
char data[2048];

// #define OPTION(t, p)
//     { t, offsetof(struct options, p), 1 }
// static const struct fuse_opt option_spec[] = {
// 	OPTION("--name=%s", filename),
// 	OPTION("--contents=%s", contents),
// 	OPTION("-h", show_help),
// 	OPTION("--help", show_help),
// 	FUSE_OPT_END
// };

int connectServer(){
		clientSocket = socket(AF_INET, SOCK_STREAM, 0);
		if(clientSocket < 0){
			printf("[-]Error in connection.\n");
			exit(1);
		}

		memset(&serverAddr, '\0', sizeof(serverAddr));
		serverAddr.sin_family = AF_INET;
		serverAddr.sin_port = htons(PORT);
		serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

		ret = connect(clientSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
		if(ret < 0){
			printf("[-]Error in connection.\n");
			exit(1);
		}
		printf("[+]Connected to Server.\n");
		struct timeval tv = {5, 0};
		setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&tv, sizeof(struct timeval));


	return 1;
}

// int getList (int ch, const char *path) {
// 	connectServer();

// 	int i,j=1,k=0;
// 	snprintf(data, sizeof(data),"[%d]{%s}",ch,path);
// 	send(clientSocket, data, sizeof(data), 0);
// 	int res = recv(clientSocket, data, sizeof(data), 0);



// 			char* token = strtok(data, "[]");
// 			i = token[0] - '0';
// 			num_of_files = i;
// 			f_ptr = malloc( i*sizeof(f_ptr));
// 			int a = 1;

// 	while (i > 0) {
// 		token = strtok(NULL, "[]");
// 		f_ptr[a].filename = malloc( 16*sizeof(char));
// 		strcpy(f_ptr[a].filename, token);
// 		a++;
// 		i--;
// 	}

// 	for(int a = 0; a<num_of_files; a++){
// 	 	printf("\n\t%s \n",f_ptr[a].filename);
// 	}

// 	return res;
// }

static int do_getattr( const char *path, struct stat *st )
{

// 	[+]Attributes of /.Trash requested
// [+]Attributes of /.Trash-1000 requested
// [+]Attributes of /.xdg-volume-info requested
// [+]Attributes of /autorun.inf requested
// THESES ARE INITIAL REQS , IN ORDER TO SKIP THIS WE ARE DOING FOLLOWING
	if(num_of_files<4){
	 	num_of_files++;
		st->st_mode = S_IFREG | 644;
		st->st_nlink = 1;
		st->st_size = 1024;
		st->st_uid = getuid(); // The owner of the file/directory is the user who mounted the filesystem
		st->st_gid = getgid(); // The group of the file/directory is the same as the group of the user who mounted the filesystem
		st->st_atime = time( NULL ); // The last "a"ccess of the file/directory is right now
		st->st_mtime = time( NULL );
		return 0;
	}
	connectServer();
	char isDir[2], c[16], a[16];
	snprintf(data, sizeof(data),"[%d]{%s}",2,path);
	send(clientSocket, data, sizeof(data), 0);
	printf( "[+]Attributes of %s requested\n", path );
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"-1")==0){
		printf("[-]Received data -1 for %s \n",path);
		st->st_mode = S_IFREG | 644;
		st->st_nlink = 1;
		st->st_size = 1024;
		st->st_uid = getuid(); // The owner of the file/directory is the user who mounted the filesystem
		st->st_gid = getgid(); // The group of the file/directory is the same as the group of the user who mounted the filesystem
		st->st_atime = time( NULL ); // The last "a"ccess of the file/directory is right now
		st->st_mtime = time( NULL );
		return -ENOENT;
	}
	char* token = strtok(data,"[;");
	strcpy(isDir,token);
	token = strtok(NULL,";;");
	int size = atoi(token);
	token = strtok(NULL,";;");
	int permOctalFormat = atoi(token);
	// token = strtok(NULL,";;");
	// strcpy(c, token);
	// token = strtok(NULL,";]");
	// strcpy(a, token);
	if(strcmp(isDir,"d")==0){
		st->st_mode = S_IFDIR | 0755;
		st->st_nlink = 2;
	}
	else {
		st->st_mode = S_IFREG | 0644;
		st->st_nlink = 1;
		st->st_size = size;
	}
	st->st_uid = getuid(); // The owner of the file/directory is the user who mounted the filesystem
	st->st_gid = getgid(); // The group of the file/directory is the same as the group of the user who mounted the filesystem
	st->st_atime = time( NULL ); // The last "a"ccess of the file/directory is right now
	st->st_mtime = time( NULL ); // The last "m"odification of the file/directory is right now

	return 0;


}

static int do_readdir( const char *path, void *buffer, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi )
{
	connectServer();
	snprintf(data, sizeof(data),"[%d]{%s}",1,path);
	send(clientSocket, data, sizeof(data), 0);
	printf("[+]Readind dir %s\n",path);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(res<0){
		printf("[-]No response from server for read dir.\n");
		return 1;
	}

	// filler( buffer, ".", 0, 0 );
	// filler( buffer, "..", 0, 0 );

	char* token = strtok(data, "[]");
	int file_count = token[0] - '0';
	for(int i=1; i<=file_count; i++){
		token = strtok(NULL, "[]");
		filler( buffer, token, NULL, 0 );

	}

	return 0;
}

static int do_read( const char *path, char *buffer, size_t size, off_t offset, struct fuse_file_info *fi )
{
	connectServer();
	printf( "[+]Read file called\n");
	printf( "\tTrying to read %s, %lu, %lu\n", path, offset, size );

	char *selectedText = NULL;
	char* file_read;
	int i=0;

	snprintf(data, sizeof(data),"[%d]{%s}",4,path);
	send(clientSocket, data, sizeof(data), 0);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"-1")==0||res<0){
		printf("[-]Unable to read file %s",path);
		return -1;
	}
	else{
		int fSize = atoi(data);
		char fbuf[65536],fbuf2[65536];
		fbuf2[0] = '\0';
		//file_read = calloc(fSize,sizeof(char));
		int res;

		while(fSize>0){
			res = recv(clientSocket, fbuf, sizeof(fbuf), 0);
			// if(res<1){
			// 	printf("[-] read res = %d buf = %s\n",res,fbuf);
			// 	fSize = -1;
			// 	continue;
			// }
			//strcpy(fbuf2,fbuf);
			//	 if(strcmp(fbuf,"eof-1")==0){
			//	 	break;
			//	 }
			i += res;
			fSize -= res;
			strcat(fbuf2,fbuf);
		}
		printf("File content of %s is - %s res = %d \n",path,fbuf2,i);
		memcpy( buffer, fbuf2, strlen(fbuf2) );
	}
	return i - offset;
}

static int do_mkdir(const char *path, mode_t mode)
{
	connectServer();
	printf("[+]MKDIR called--------%s\n",path);
	snprintf(data, sizeof(data),"[%d]{%s}",3,path);
	send(clientSocket, data, sizeof(data), 0);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"1")==0){
		printf("[+] Directory created %s",path);
		return 0;
	}
	else{
		printf("[-] Directory not created %s",path);
		return 1;
	}
}



static int do_rename (const char *path, const char *buffer){
	connectServer();
	printf("[+]Renaming file name\n");
	snprintf(data, sizeof(data),"[%d]{%s}{%s}",5,path,buffer);
	send(clientSocket, data, sizeof(data), 0);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"1")==0){
		printf("[+] File name changed ..client");
		return 0;
	}
	else{
		printf("[-] File name not changed...client ");
		return 1;
	}





	return 0;
}


static int do_rmdir(const char *path)
{
	connectServer();
	printf("[+]RMDIR called--------%s\n",path);
	snprintf(data, sizeof(data),"[%d]{%s}",6,path);
	send(clientSocket, data, sizeof(data), 0);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"1")==0){
		printf("[+] Directory Removed %s",path);
		return 0;
	}
	else{
		printf("[-] Directory could not be removed...(not empty) %s",path);
		return 1;
	}
}

static int do_unlink(const char *path)
{
	connectServer();
	printf("[+]UNLINK called--------%s\n",path);
	snprintf(data, sizeof(data),"[%d]{%s}",7,path);
	send(clientSocket, data, sizeof(data), 0);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"1")==0){
		printf("[+] File Removed %s \n",path);
		return 0;
	}
	else{
		printf("[-]File not removed %s \n",path);
		return 1;
	}
}

static int do_symlink(const char *path,const char *buffer)
{
	connectServer();
	printf("[+]SYMLINK called--------%s\n",path);
	snprintf(data, sizeof(data),"[%d]{%s}{%s}",8,path,buffer);
	send(clientSocket, data, sizeof(data), 0);
	int res = recv(clientSocket, data, sizeof(data), 0);
	if(strcmp(data,"1")==0){
		printf("[+] File Linked %s \n",path);
		return 0;
	}
	else{
		printf("[-]File not linked %s \n",path);
		return 1;
	}


}



static struct fuse_operations operations = {
    	.getattr	= do_getattr,
    	.readdir	= do_readdir,
    	.read	= do_read,
	.mkdir = do_mkdir,
	.rename = do_rename,
	.rmdir  =  do_rmdir,
	.unlink = do_unlink,
	.symlink = do_symlink,
};

static void show_help(const char *progname)
{
	printf("usage: %s [options] <mountpoint>\n\n", progname);
	printf("File-system specific options:\n"
	       "    --name=<s>          Name of the \"hello\" file\n"
	       "                        (default: \"hello\")\n"
	       "    --contents=<s>      Contents \"hello\" file\n"
	       "                        (default \"Hello, World!\\n\")\n"
	       "\n");
}

int main( int argc, char *argv[] )
{
	// struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

	// options.filename = strdup("Mihir Welcomes you");
	// options.contents = strdup("Hello World!\n");

	// if (options.show_help) {
	// 	show_help(argv[0]);
	// 	assert(fuse_opt_add_arg(&args, "--help") == 0);
	// 	args.argv[0] = (char*) "";
	// }

	return fuse_main( argc, argv, &operations, NULL );
}
