Build started at 2019-01-24T20:48:57.754091
Main binary: /usr/bin/python3
Python system: Linux
The Meson build system
Version: 0.45.1
Source dir: /home/mihir/Desktop/fuse-3.2.6
Build dir: /home/mihir/Desktop/fuse-3.2.6/build
Build type: native build
Project name: libfuse3
Sanity testing C compiler: cc
Is cross compiler: False.
Sanity check compiler command line: cc /home/mihir/Desktop/fuse-3.2.6/build/meson-private/sanitycheckc.c -o /home/mihir/Desktop/fuse-3.2.6/build/meson-private/sanitycheckc.exe
Sanity check compile stdout:

-----
Sanity check compile stderr:

-----
Running test binary command: /home/mihir/Desktop/fuse-3.2.6/build/meson-private/sanitycheckc.exe
Native C compiler: cc (gcc 7.3.0 "cc (Ubuntu 7.3.0-27ubuntu1~18.04) 7.3.0")
Build machine cpu family: x86_64
Build machine cpu: x86_64
Running compile:
Working directory:  /tmp/tmpfqxzxf6r
Command line:  cc /tmp/tmpfqxzxf6r/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpfqxzxf6r/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_fork || defined __stub___fork
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &fork;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "fork": YES
Running compile:
Working directory:  /tmp/tmp7ozd4fsb
Command line:  cc /tmp/tmp7ozd4fsb/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmp7ozd4fsb/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_fstatat || defined __stub___fstatat
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &fstatat;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "fstatat": YES
Running compile:
Working directory:  /tmp/tmpbufbjm6o
Command line:  cc /tmp/tmpbufbjm6o/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpbufbjm6o/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_openat || defined __stub___openat
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &openat;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "openat": YES
Running compile:
Working directory:  /tmp/tmp0flsfu9m
Command line:  cc /tmp/tmp0flsfu9m/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmp0flsfu9m/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_readlinkat || defined __stub___readlinkat
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &readlinkat;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "readlinkat": YES
Running compile:
Working directory:  /tmp/tmpxba8of_d
Command line:  cc /tmp/tmpxba8of_d/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpxba8of_d/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_pipe2 || defined __stub___pipe2
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &pipe2;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "pipe2": YES
Running compile:
Working directory:  /tmp/tmprsukwg6f
Command line:  cc /tmp/tmprsukwg6f/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmprsukwg6f/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_splice || defined __stub___splice
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &splice;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "splice": YES
Running compile:
Working directory:  /tmp/tmpt_zaq55r
Command line:  cc /tmp/tmpt_zaq55r/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpt_zaq55r/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_vmsplice || defined __stub___vmsplice
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &vmsplice;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "vmsplice": YES
Running compile:
Working directory:  /tmp/tmpam2oivtj
Command line:  cc /tmp/tmpam2oivtj/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpam2oivtj/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_posix_fallocate || defined __stub___posix_fallocate
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &posix_fallocate;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "posix_fallocate": YES
Running compile:
Working directory:  /tmp/tmp_28czpoe
Command line:  cc /tmp/tmp_28czpoe/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmp_28czpoe/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_fdatasync || defined __stub___fdatasync
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &fdatasync;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "fdatasync": YES
Running compile:
Working directory:  /tmp/tmpxjw2yhtz
Command line:  cc /tmp/tmpxjw2yhtz/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpxjw2yhtz/output.exe 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>

        #if defined __stub_utimensat || defined __stub___utimensat
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &utimensat;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "utimensat": YES
Running compile:
Working directory:  /tmp/tmp1uwd8tg0
Command line:  cc /tmp/tmp1uwd8tg0/testfile.c -O0 -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmp1uwd8tg0/output.exe 

Code:
 #include <sys/xattr.h>
#include <limits.h>

        #if defined __stub_setxattr || defined __stub___setxattr
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &setxattr;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "setxattr": YES
Running compile:
Working directory:  /tmp/tmp1wcnzy6y
Command line:  cc /tmp/tmp1wcnzy6y/testfile.c -O0 -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmp1wcnzy6y/output.exe 

Code:
 #include <iconv.h>
#include <limits.h>

        #if defined __stub_iconv || defined __stub___iconv
        fail fail fail this function is not going to work
        #endif
        
int main() {
            void *a = (void*) &iconv;
            long b = (long) a;
            return (int) b;
        }
Compiler stdout:
 
Compiler stderr:
 
Checking for function "iconv": YES
Running compile:
Working directory:  /tmp/tmpj15j7x_i
Command line:  cc /tmp/tmpj15j7x_i/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -c -o /tmp/tmpj15j7x_i/output.obj 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

        void bar() {
            struct stat foo;
            foo.st_atim;

        };
Compiler stdout:
 
Compiler stderr:
 
Checking whether type "struct stat" has member "st_atim": YES
Running compile:
Working directory:  /tmp/tmp6it4sx8i
Command line:  cc /tmp/tmp6it4sx8i/testfile.c -O0 -D_GNU_SOURCE -pipe -D_FILE_OFFSET_BITS=64 -c -o /tmp/tmp6it4sx8i/output.obj 

Code:
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

        void bar() {
            struct stat foo;
            foo.st_atimespec;

        };
Compiler stdout:
 
Compiler stderr:
 /tmp/tmp6it4sx8i/testfile.c: In function ‘bar’:
/tmp/tmp6it4sx8i/testfile.c:12:17: error: ‘struct stat’ has no member named ‘st_atimespec’; did you mean ‘st_atim’?
             foo.st_atimespec;
                 ^~~~~~~~~~~~
                 st_atim

Checking whether type "struct stat" has member "st_atimespec": NO
Configuring config.h using configuration
Running compile:
Working directory:  /tmp/tmp7ubjypq7
Command line:  cc /tmp/tmp7ubjypq7/testfile.c -O0 -O0 -Werror=unused-result -pipe -D_FILE_OFFSET_BITS=64 -c -o /tmp/tmp7ubjypq7/output.obj 

Code:
 
__attribute__((warn_unused_result)) int get_4() {
    return 4;
}
int main(void) {
    (void) get_4();
    return 0;
}
Compiler stdout:
 
Compiler stderr:
 /tmp/tmp7ubjypq7/testfile.c: In function ‘main’:
/tmp/tmp7ubjypq7/testfile.c:6:5: error: ignoring return value of ‘get_4’, declared with attribute warn_unused_result [-Werror=unused-result]
     (void) get_4();
     ^~~~~~~~~~~~~~
cc1: some warnings being treated as errors

Message: Compiler warns about unused result even when casting to void
Dependency threads found: YES
Running compile:
Working directory:  /tmp/tmpzf585cu1
Command line:  cc /tmp/tmpzf585cu1/testfile.c -O0 -Wl,--start-group -ldl -Wl,--end-group -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpzf585cu1/output.exe 

Code:
 int main(int argc, char **argv) { return 0; }
Compiler stdout:
 
Compiler stderr:
 
Library dl found: YES
Running compile:
Working directory:  /tmp/tmpnriape8_
Command line:  cc /tmp/tmpnriape8_/testfile.c -O0 -Wl,--start-group -lrt -Wl,--end-group -pipe -D_FILE_OFFSET_BITS=64 -o /tmp/tmpnriape8_/output.exe 

Code:
 int main(int argc, char **argv) { return 0; }
Compiler stdout:
 
Compiler stderr:
 
Library rt found: YES
Adding test "wrong_cmd".
Build targets in project: 26
Found ninja-1.8.2 at /usr/bin/ninja
