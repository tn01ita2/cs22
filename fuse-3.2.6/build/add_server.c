#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <dirent.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <time.h>
#include <fcntl.h>


#define PORT 4444
//#define root "/home/micro/Desktop/FUSETEST"
#define root "/home/mihir/Desktop/FUSETEST"

char fStat[1024];
char buffer[2048];
char path[1024];

void optionSwitch(char buf[1024], int newSocket);

int main(){

	int sockfd, ret;
	struct sockaddr_in serverAddr;

	int newSocket;
	struct sockaddr_in newAddr;

	socklen_t addr_size;

	pid_t childpid;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0){
		printf("[-]Error in connection.\n");
		exit(1);
	}
	printf("[+]Server Socket is created.\n");

	memset(&serverAddr, '\0', sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	serverAddr.sin_port = htons(PORT);
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	ret = bind(sockfd, (struct sockaddr*)&serverAddr, sizeof(serverAddr));
	if(ret < 0){
		printf("[-]Error in binding.\n");
		exit(1);
	}
	printf("[+]Bind to port %d\n", PORT);

	if(listen(sockfd, 10) == 0){
		printf("[+]Listening....\n");
	}else{
		printf("[-]Error in listen.\n");
	}


	while(1){

		newSocket = accept(sockfd, (struct sockaddr*)&newAddr, &addr_size);
		if(newSocket < 0){
			printf("[-] newSocket < 0 Connetion Terminated\n");
			continue;
		}
		if((childpid = fork()) == 0){
			//printf("[+]Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
			recv(newSocket, buffer, sizeof(buffer), 0);
			//printf("[+]Data %s\n", buffer);
			optionSwitch(buffer, newSocket);

		}
		else{
			close(newSocket);
		}
	}
	return 0;
}

void optionSwitch(char buf[1024], int newSocket){
	char data[1024], dirPath[1024];
	struct stat fileStat;
	data[0]='\0';
	int ch, k=0;
	strcpy(path,root);
	char* token = strtok(buf, "[]");
	ch = token[0] - '0';
	token = strtok(NULL, "{}");
	strcat(path, token);
	DIR *d;
	struct dirent *dir;
	d = opendir(path);
	switch(ch){
			case 1:
					if (d)
					{
						while ((dir = readdir(d)) != NULL)
						{
						// 	if((strcmp(dir->d_name, "..") != 0)&&(strcmp(dir->d_name, ".")))
					  	// {
							k++;
							strcat(data, "[");
							strcat(data, dir->d_name);
							strcat(data, "]");
						//}
							// 	k++;
							// 	strcpy(dirPath, "/home/mihir/Desktop/FUSETEST/");
							// 	strcat(dirPath, dir->d_name);
							// 	char * err = fileStat(dirPath, dir->d_name);
							// 	if(strcmp(err,"exit")==0){
							// 		exit;
							// 	}
							// 	else{
							// 		strcat(data, err);
							// 	}

							// }
						}
						closedir(d);
					}
					else{
						printf("[-] Unable to locate path %s\n", path);
					}
					snprintf(buffer, sizeof(buffer),"[%d]%s",k,data);
					//printf("[+]  list of files  %s\n", buffer);
					send(newSocket, buffer, sizeof(buffer), 0);
			break;
			case 2:
				printf("[+] Finding atrributes of  %s\n", path);
				if(stat(path, &fileStat) < 0){
					printf("[-]Unable to find file %s\n", path);
					strcpy(buffer,"-1");
				}
				else{
					int size, permOctalFormat;
					size = fileStat.st_size;
					permOctalFormat = fileStat.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO);
					if(S_ISDIR(fileStat.st_mode)){
						snprintf(buffer, sizeof(buffer), "[d;%d;%o;%s;%s]",size,permOctalFormat,ctime(&fileStat.st_mtime), ctime(&fileStat.st_atime));
					}
					else{
						snprintf(buffer, sizeof(buffer), "[-;%d;%o;%s;%s]",size,permOctalFormat,ctime(&fileStat.st_mtime), ctime(&fileStat.st_atime));
					}
				}
				send(newSocket, buffer, sizeof(buffer), 0);
			break;
			case 3:
				printf("[+] Inside create directory %s\n",path);

				if(mkdir(path, 0755) == -1){
				printf("[-] Unable to create directory %s\n",path);
				strcpy(buffer,"-1");
				send(newSocket, buffer, sizeof(buffer), 0);
				}
				else{
					printf("[+] Directory created %s\n",path);
					strcpy(buffer,"1");
					send(newSocket, buffer, sizeof(buffer), 0);
				}
				break;
			case 4:
				if(stat(path, &fileStat) < 0){
					printf("[-]Unable to find file %s\n", path);
					strcpy(buffer,"-1");
					send(newSocket, buffer, sizeof(buffer), 0);
					break;
				}
				else{
					char  *buf_read;
					FILE *fd;
					fd = fopen(path, "r");
					// fseek(fd, 0L, SEEK_END);
					// long numbytes = ftell(fd);
					// fseek(fd, 0L, SEEK_SET);
					int size = fileStat.st_size;
					buf_read = malloc((size + 1)*sizeof(char));
					if(buf_read == NULL)
					{
						printf("[-] zero byte file %s \n",path);
						strcpy(buffer,"-1");
						send(newSocket, buffer, sizeof(buffer), 0);
						break;
					}
					int bytes_read = fread(buf_read, 1, size, fd);
					printf("bytes read %d and size %d\n",bytes_read,size);
					fclose(fd);
					if((strlen(buf_read)<1) || (bytes_read < 1)){
						printf("[-] zero byte file %s \n",path);
						strcpy(buffer,"-1");
						send(newSocket, buffer, sizeof(buffer), 0);
						break;
					}
					snprintf(buffer, sizeof(buffer),"%d",size+1);
					send(newSocket, buffer, sizeof(buffer), 0);
					char *p = buf_read;
					int i = 1;
					while (size > 0) {
						int bytes_written = send(newSocket, p, strlen(buf_read), 0 );
						if (bytes_written <= 0) {
							printf("[-] zero byte file %s \n",path);
							strcpy(buffer,"-1");
							send(newSocket, buffer, sizeof(buffer), 0);
							break;
						}
						printf("[+] sent %s bytes read %d times %d \n",buf_read,bytes_written, i);
						i++;
						size -= bytes_written;
						p += bytes_written;
					}
					//snprintf(buffer, sizeof(buffer),"%s","eof-1");
					// send(newSocket, buffer, sizeof(buffer), 0);
					free(buf_read);
				}


			break;
			case 5:
				{	printf("Inside server rename function");
					char new[64];
					strcpy(new,root);
					token = strtok(NULL, "{}");
					strcat(new,token);
					int x = rename(path,new);
					if(x==-1){
						printf("renaming of file failed..server");
						strcpy(buffer,"-1");
						send(newSocket, buffer, sizeof(buffer), 0);
						}
					else
						{
						printf("renaming of file done...server");
						 strcpy(buffer,"1");
						 send(newSocket, buffer, sizeof(buffer), 0);
						}

				}

			break;

			case 6:
				{
					printf("[+]Inside rmdir function at server\n");
					if(rmdir(path) == -1)
					{
						printf("[-]Unable to delete... Directory not empty %s\n",path);
						strcpy(buffer,"-1");
						send(newSocket, buffer, sizeof(buffer), 0);
					}
				else
					{
						printf("[+] Directory Removed ");
						strcpy(buffer,"1");
						send(newSocket, buffer, sizeof(buffer), 0);

					}

			break;
			}

			case 7:

					printf("[+]Inside Unlink function at server\n");
					if(unlink(path) == -1)
					{
						printf("[-]Unable to delete the file %s\n",path);
						strcpy(buffer,"-1");
						send(newSocket, buffer, sizeof(buffer), 0);
					}
					else
					{
						printf("[+] File Removed %s",path);
						strcpy(buffer,"1");
						send(newSocket, buffer, sizeof(buffer), 0);

					}

				  break;

			  default:
			  break;
			}

}
// char * statsOfFile(char * path, char * fName){
// 	struct stat fileStat;
//     if(stat(path, &fileStat) < 0){
// 	    printf("[-]Unable to find file %s\n", path);
// 		return "exit";
// 	}
// 	int size, nLinks, inode, permOctalFormat;
// 	size = fileStat.st_size;
// 	nLinks = fileStat.st_nlink;
// 	inode = fileStat.st_ino;
// 	permOctalFormat = fileStat.st_mode & (S_IRWXU | S_IRWXG | S_IRWXO);
// 	// printf("chmod: %o\n", permOctalFormat);
// 	// printf("File Permissions: \t");
//     // printf( (S_ISDIR(fileStat.st_mode)) ? "d" : "-");
//     // printf( (fileStat.st_mode & S_IRUSR) ? "r" : "-");
//     // printf( (fileStat.st_mode & S_IWUSR) ? "w" : "-");
//     // printf( (fileStat.st_mode & S_IXUSR) ? "x" : "-");
//     // printf( (fileStat.st_mode & S_IRGRP) ? "r" : "-");
//     // printf( (fileStat.st_mode & S_IWGRP) ? "w" : "-");
//     // printf( (fileStat.st_mode & S_IXGRP) ? "x" : "-");
//     // printf( (fileStat.st_mode & S_IROTH) ? "r" : "-");
//     // printf( (fileStat.st_mode & S_IWOTH) ? "w" : "-");
//     // printf( (fileStat.st_mode & S_IXOTH) ? "x" : "-");
// 	// printf("The file %s a symbolic link\n", (S_ISLNK(fileStat.st_mode)) ? "is" : "is not");

// 	if(strcmp(fName, ".") == 0){
// 		//snprintf(fStat ,sizeof(fStat), ".{%o}",permOctalFormat);
// 	}
// 	else if(S_ISDIR(fileStat.st_mode)){
// 		snprintf(fStat, sizeof(fStat), "{%s;d;%d;%o}",fName,size,permOctalFormat);
// 	}
// 	else{
// 		snprintf(fStat, sizeof(fStat), "{%s;-;%d;%o}",fName,size,permOctalFormat);
// 	}
// 	return (char *) statsOfFile;
// }
